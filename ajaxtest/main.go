package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/star191/goweb/ajaxtest/handlers"
)

func main() {

	ctrl := handlers.CtrlCreate()
	InitHandlers(ctrl)
	var srv *http.Server

outer:
	for {
		srv, _ = StartHttpServer(ctrl)
	inner:
		for {

			select {
			case <-ctrl["stop"]:
				time.Sleep(3 * time.Second)
				shutdownServer(srv)

				break outer

			case <-ctrl["restart"]:

				shutdownServer(srv)
				time.Sleep(3 * time.Second)

				break inner

			}
		}
	}

	for _, ch := range ctrl {
		close(ch)
	}

}

func InitHandlers(ctrl handlers.Ctrls) {
	http.HandleFunc("/hand", handlers.MainHandler)
	http.HandleFunc("/async", handlers.AsyncHandler)
	http.HandleFunc("/ws", handlers.WSHandler)
	http.Handle("/", http.FileServer(http.Dir("./public/")))
	handlers.ShutdownHandler(ctrl)
	handlers.RestartHandler(ctrl)
}

func StartHttpServer(ctrl handlers.Ctrls) (*http.Server, error) {
	fmt.Println("Start server...")

	srv := &http.Server{Addr: ":8080"}

	var err error
	go func() {

		err = srv.ListenAndServe()

		if err != nil {
			fmt.Println(err)
		}
	}()

	fmt.Println("Server is running")

	return srv, err
}

func shutdownServer(srv *http.Server) error {

	ctx := context.Background()

	err := srv.Shutdown(ctx)

	return err
}
